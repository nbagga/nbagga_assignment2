package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

   /* @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }*/
    DotsAndBoxesGrid dabg;

    @BeforeEach
    void setUp() {
        dabg = new DotsAndBoxesGrid(3, 3, 1);
    }
    @Test
    public void testIsBoxComplete() {

        logger.info("Test for complete boxes");

        // draw first complete box
        dabg.drawHorizontal(0,0,1);
        dabg.drawHorizontal(0,1,1);
        dabg.drawVertical(0,0,1);
        dabg.drawVertical(1,0,1);

        // draw second incomplete box
        //dabg.drawHorizontal(0,1,1);

        // checking that the first complete box gets true from boxComplete method
        assertEquals(

                true, dabg.boxComplete(0,0)

        );

        // checking that the second incomplete box gets false from boxComplete method
        assertEquals(

                false, dabg.boxComplete(1,0)

        );
    }
    @Test
    public void testduplinehorizonatal() {
        logger.info("Test for duplicate horizontal line");
        dabg.drawHorizontal(0, 0, 1);
        dabg.drawVertical(0, 0, 1);


        assertThrows(IllegalStateException.class, () -> {
            dabg.drawHorizontal(0,0,1);
        });
    }
    @Test
    public void testduplinevertical() {
        logger.info("Test for duplicate vertical line");
        dabg.drawHorizontal(0, 0, 1);
        dabg.drawVertical(0, 0, 1);


        assertThrows(IllegalStateException.class, () -> {
            dabg.drawVertical(0,0,1);
        });
    }
}
